# Actuator Characterization

medium-ok haiku about machines:

*find the boundary*
*between component, machine*
*only there in dreams*

Trying to understand breakdown of available actuators, relating to power density, torque density, cost w/r/t both of these, complexity of installation etc. Things are made from parts, we have been using the same set of parts to make robotics for decades, can we find the actuator parameter-spaces that are unpopulated, but should be? What limits are based on the physics (max mag force / permeability, magnetic strength, coil limitations etc - max piezoelectric stress - etc) and which might be based on design?

For example:
 - can changes to stepper drive algorithms (fast microcode) push the roof on stepper motor power density? (FastFOC w/ GaN FET, what is inductance switching limit?)
 - can an integration of gear reduction and electromagnetic drive put an actuator in new parameter space?

# About the Measurements

When compiling data.csv, I'm largely looking through motor suppliers that I'm familiar with and pulling specifications together from datasheets. Wherever possible, I try to look for groups of similar motors from similar manufacturers so that test methods etc can be assumed to be somewhat similar. 

On power measurements: characterizing torque, speed, and total power is kind of tricky because these things tend to be speed dependent and nonstraightforward - they have character. For Stepper Motors I'm looking at speed-torque 'pull out' curves and calculating power by $'P = T\omega'$ (Power (w) is Torque (Nm) * Angular Velocity (rads/s)). Or, because I find most speed # in RPM: $'P = T\pi n_{rpm}/30'$ . This happens at some point on the speed curve, so I'll try to record the speed where this happens in the data as well. For BLDC's, I'm basically just reading right off of the spec'd power numbers. There's a few rabbit holes to go down here - trying to characterize motor constants, etc, but I'm trying to get a good overview of a really broad range of *actuators* not just motors, so here we are. 

Many motors have Peak Torques and Continuous Torques - they can be driven with very high current for a few moments (10-15s typically) before they overheat. So these two measurements are included where possible, where no data is available both numbers will be the same.

For most of the BLDC's specified (almost all from Hobbyking) I take three values available across most motors: the max. power rating (which I assume has to due largely to heat dissipation), the maximum current rating, and the motor's Kv constant - this is expressed normally as 'RPMs per Volt' but motor physics is strange, and this is inversely related to the Kq, or torque constant - we can imagine that if a motor spins generally slower at the same voltage with the same power output, we must have more torque. To convert this, I use $'Kq = 30/(PI * Kv)'$' - a kind of colloquial term.

Some other things to consider with the BLDCs - the weight does not account for control and sensing circuitry, which is admittedly small, but is included with the Teknic Motors. In addition, it's likely that these motors are harder to perform servo-control with, as they have (generally) lower pole counts. However, this is also one of the points to make: that better embedded systems allow weird things to be done with control to make 'new' types of actuators.