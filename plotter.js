// from https://plot.ly/javascript/dropdowns/

// NEXT: aggregate density and cost-specific datas with math
// Data: Wants Gearhead Actuators: harmonic + stepper, harmonic + bldc, planetary + both
// Data: BLDC Motors

Plotly.d3.csv('data.csv', function(err, rows){

    // get array of this key
    function unpack(rows, key) {
        return rows.map(function(row) { return row[key]; });
    }

    console.log(rows);

    for (var i = rows.length - 1; i >= 0; i--) {
      rows[i]["Specific Torque (N*m / kg)"] = rows[i]["Constant Torque (N*m)"] / rows[i]["Weight (kg)"];
      rows[i]["Cost Specific Torque (N*m / USD)"] = rows[i]["Constant Torque (N*m)"] / rows[i]["Cost (USD)"];
      rows[i]["Specific Power (w / kg)"] = rows[i]["Power (W)"] / rows[i]["Weight (kg)"];
    }

    // for sorting
    var allSetNames = unpack(rows, 'Set'),
        listofSets = [],
        chosenSets = [],
        listofValues = Object.getOwnPropertyNames(rows[0]),
        traces = []; // of traces, for re-drawing

    console.log(rows);

    // find unique set names
    for (var i = 0; i < allSetNames.length; i++ ){
      if (listofSets.indexOf(allSetNames[i]) === -1 ){
          listofSets.push(allSetNames[i]);
      }
    }

    // Default data
    chosenSets = ['Steppers', 'Servos', 'BLDCs', 'BLDC GM', 'BLDC HD', 'BLDC HD-A'];
    setChart(listofValues[8], listofValues[9]);

    function setChart(chosenXStat, chosenYStat) {

      traces = [];

        for (var i = chosenSets.length - 1; i >= 0; i--) {

          var xdata = [];
          var ydata = [];
          var names = [];
          for (var j = allSetNames.length - 1; j >= 0; j--) {
            if(allSetNames[j] === chosenSets[i]){
              xdata.push(unpack(rows, chosenXStat)[j]);
              ydata.push(unpack(rows, chosenYStat)[j]);
              names.push(unpack(rows, 'Name')[j]);
            }
          }

          var trace = {
            x: xdata,
            y: ydata,
            name: chosenSets[i],
            mode: 'markers+text',
            text: names,
            textposition: 'bottom center',
            marker: {
              size: 12,
              opacity: 0.5,
            }
          };

          traces.push(trace);
        }

        var layout = {
            title:'modplot',
            height: 1000,
            width: 1000,
            xaxis: {
              type:'log',
              autorange: true,
              title:'x'
            },
            yaxis: {
              type:'log',
              autorange: true,
              title:'y'
            }
        };

        Plotly.newPlot('plotdiv', traces, layout);
    };

    // some DOM
    var innerContainer = document.querySelector('[data-num="0"'),
        plotEl = innerContainer.querySelector('.plot'),
        xSelector = innerContainer.querySelector('.xdropdown'),
        ySelector = innerContainer.querySelector('.ydropdown');
        setSelector = document.getElementById('setdiv');

    // write options into dropdowns
    function assignOptions(textArray, div) {
        for (var i = 2; i < textArray.length;  i++) {
            var currentOption = document.createElement('option');
            currentOption.text = textArray[i];
            div.appendChild(currentOption);
        }
    }

    function makeCheckboxes(textArray, div){
      for (var i = textArray.length - 1; i >= 0; i--) {
        var button = document.createElement("button");
        button.innerHTML = textArray[i];
        button.addEventListener("click", toggleSet, false)
        div.appendChild(button);
      }
    }

    assignOptions(listofValues, xSelector);
    assignOptions(listofValues, ySelector);
    makeCheckboxes(listofSets, setSelector);
    xSelector.value = listofValues[8];
    ySelector.value = listofValues[9];

    // update after dropdown
    function updateChart(){
        setChart(xSelector.value, ySelector.value);
    }

    function toggleSet(event){
      var setName = event.explicitOriginalTarget.innerHTML;
      if(chosenSets.includes(setName)){
        var index = chosenSets.indexOf(setName);
        if(index > -1){
          chosenSets.splice(index, 1);
        }
      } else {
        chosenSets.push(setName);
      }
      updateChart();
    }

    xSelector.addEventListener('change', updateChart, false);
    ySelector.addEventListener('change', updateChart, false);

});